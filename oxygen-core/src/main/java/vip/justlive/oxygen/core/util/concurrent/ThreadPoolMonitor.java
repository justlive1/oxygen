/*
 * Copyright (C) 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package vip.justlive.oxygen.core.util.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import vip.justlive.oxygen.core.CoreConfigKeys;
import vip.justlive.oxygen.core.Plugin;
import vip.justlive.oxygen.core.util.timer.WheelTimer;

/**
 * 线程池监控
 *
 * @author wubo
 */
@Slf4j
public class ThreadPoolMonitor implements Plugin {

  private static final List<ThreadPoolExecutor> POOLS = new ArrayList<>();
  private static final List<WheelTimer> TIMERS = new ArrayList<>();

  private final RepeatRunnable threadMonitor = new RepeatRunnable("Thread-monitor", this::monitor);

  public static void add(ThreadPoolExecutor pool) {
    POOLS.add(pool);
  }

  public static void add(WheelTimer timer) {
    TIMERS.add(timer);
  }

  public static void remove(ThreadPoolExecutor pool) {
    POOLS.remove(pool);
  }

  @Override
  public void start() {
    boolean b = CoreConfigKeys.THREAD_POOL_MONITOR_ENABLED.castValue(boolean.class);
    log.info("start thread pool monitor, enabled={}", b);
    if (b) {
      ThreadUtils.residentPool().add(threadMonitor);
    }
  }

  @Override
  public void stop() {
    log.info("stop thread pool monitor");
    if (CoreConfigKeys.THREAD_POOL_MONITOR_ENABLED.castValue(boolean.class)) {
      threadMonitor.shutdown();
    }
    POOLS.clear();
    TIMERS.clear();
  }

  private void monitor() {
    log.info("report pool status begin.");
    log.info("summary: ThreadPoolExecutor total size: {}, WheelTimer total size: {}", POOLS.size(),
        TIMERS.size());
    for (ThreadPoolExecutor pool : POOLS) {
      log.info("ThreadPoolExecutor status : {}", pool);
      if (pool.isShutdown()) {
        log.info("ThreadPoolExecutor [{}] is shutdown, remove from monitor list",
            Integer.toHexString(pool.hashCode()));
        POOLS.remove(pool);
      }
    }
    for (WheelTimer timer : TIMERS) {
      log.info("WheelTimer status : {}", timer);
      if (timer.isShutdown()) {
        log.info("WheelTimer [{}] is shutdown, remove from monitor list",
            Integer.toHexString(timer.hashCode()));
        TIMERS.remove(timer);
      }
    }
    log.info("report pool status end.");
    ThreadUtils.sleep(CoreConfigKeys.THREAD_POOL_MONITOR_INTERVAL.castValue(long.class),
        TimeUnit.SECONDS);
  }
}
